const fs = require('fs');
const async = require('async');
const Corpus = require('./Corpus');
const textHelper = require('./text_processing');

const args = process.argv.slice(2);
const CORPUS_DIR =  process.env.CORPUS_DIR || './corpus';

function search(queries, callback) {
	if(queries.length === 0) {
		return callback(new Error('Must supply at least one query'));
	}

	function loadCorpus(dir, callback) {
		// What are the documents' filenames?
		fs.readdir(dir, (err, filenames) => {
			if(err) return callback(err);
			if(filenames.length === 0) return callback(new Error('No documents found, aborting'));

			filenames = filenames.map(filename => {
				return `${CORPUS_DIR}/${filename}`;
			});

			function readFile(name, callback) {
				fs.readFile(name, (err, file) => {
					if(err) return callback(err);

					var terms = file.toString();
					// We convert "document_XX.txt" -> "XX" for prettier output later.
					name = Number(name.match(/[0-9]+/));
					return callback(null, { terms, name });
				});
			}

			// Read in each document.
			async.map(filenames, readFile, (err, documents) => {
				if(err) return callback(err);

				var corpus = new Corpus(documents);

				callback(null, corpus);
			});
		});
	}

	loadCorpus(CORPUS_DIR, (err, corpus) => {
		if(err) return callback(err);

		// For each query..
		results = queries
		// Tokenize it.
		.map(textHelper.tokenize)
		// Remove duplicate terms.
		.map(textHelper.removeDuplicateTerms)
		// Search the corpus for that query.
		.map(query => {
			return corpus.search(query);
		});

		callback(null, results);
	});
}

function displayResults(results) {
	for(var result of results) {
		console.log(`Documents with highest word relevance for query "${result.query}" (DESC order):`);
		console.log();

		for(var document of result.scores) {
			console.log(`- Document ${document.name}: ${document.score}`);

			for(term of document.breakdown) {
				console.log(`-- ${term.value}: ${term.score}`);
			}

			console.log();
		}
		console.log();
	}

}

search(args, (err, results) => {
	if(err) {
		console.log(err.message);
		return process.exit(1);
	}

	displayResults(results);
});
