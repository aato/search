function tokenize(str) {
	return str
	.toLowerCase()
	// Remove all possessives. Example: john's -> john.
	.replace(/([a-z]{2,})'s/g, '$1')
	// Match all runs of alphanumeric terms.
	// Example: "he was tall (180cm at only 17)" -> "he was tall 180cm at only 17".
	.match(/([a-z]|[0-9])+/g);
}

function removeDuplicateTerms(terms) {
	return terms.reduce((acc, cur) => {
		if(acc.indexOf(cur) === -1) acc.push(cur);
		return acc;
	}, []);
}

module.exports = {
	tokenize,
	removeDuplicateTerms
}