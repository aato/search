const Document = require('./Document');
const textHelper = require('./text_processing');

function Corpus(documents) {
	// For each of the passed in documents...
	this._documents = documents
	// Tokenize the raw text.
	.map(this._tokenize)
	// Create a new Document object.
	.map(data => {
		return new Document(data);
	});

	this._N = this._documents.length;
}

Corpus.prototype._tokenize = function(document) {
	document.terms = textHelper.tokenize(document.terms);
	return document;
}

// Computes word relevance of a query when searching against this corpus.
// Returns a resultset in DESC order of the form:
// ('name' is name of the document)
// {
//   query: 'some query',
//   results: [{
//     name: 5,
//     score: 0.057215246578472274
//   }, {
//     name: 1,
//     score: 0.03293781118475848
//   }, {
//     name: 19,
//     score: 0
//   }]
// }
Corpus.prototype.search = function(terms) {
	var corpus = this;
	var results = {
		query: terms.join(' '),
		scores: []
	};
	var wrMatrix = {};
	// Maps a term to the number of documents it's in.
	var nMatrix = {};

	function getWordImportance(term, document) {
		return document.getTF(term) / document.getTotalWords();
	}

	function getGeneralityDiscount(term, N) {
		var n = nMatrix[term];

		// Adding 1 to denominator in case term is not in corpus.
		return Math.log10(N / (n + 1));
	}

	// Build nMatrix.
	for(var term of terms) {
		nMatrix[term] = 0;

		for(var document of this._documents) {
			if(document.containsTerm(term)) nMatrix[term]++;
		}
	}

	// Build wrMatrix.
	for(var term of terms) {
		wrMatrix[term] = wrMatrix[term] || {};
	
		for(var document of this._documents) {
			var docName = document.getName();
			wrMatrix[term][docName] = wrMatrix[term][docName] || 0;
			
			var N = this._N;
			var wordImportance = getWordImportance(term, document);
			var generalityDiscount = getGeneralityDiscount(term, N);
			var wordRelevance = wordImportance * generalityDiscount;

			wrMatrix[term][docName] = wordRelevance;
		}
	}

	// Build resultset.
	// Specifically, for each document...
	results.scores = this._documents.map(document => {
		var docName = document.getName();
		// Compute it's wr score for the given query.
		var score = terms.reduce((acc, term) => {
			acc += wrMatrix[term][docName];
			return acc;
		}, 0);

		// Record a breakdown of how each term in the query contributed to the total wr score.
		var breakdown = terms.map(term => {
			return {
				value: term,
				score: wrMatrix[term][docName]
			}
		});

		return { name: docName, score, breakdown }
	});

	// Sort results in DESC order.
	results.scores = results.scores.sort((a, b) => {
		return b.score - a.score;
	});

	return results;
}

module.exports = Corpus;