function computeTermFrequencyTable(terms) {
	var table = {};

	// Remove duplicate terms.
	var uniqueTerms = terms.reduce((acc, cur) => {
		if(acc.indexOf(cur) === -1) acc.push(cur);
		return acc;
	}, []);

	// Record how many times each terms in mentioned in the document.
	for(var uniqueTerm of uniqueTerms) {
		var r = new RegExp(uniqueTerm, 'g');
		table[uniqueTerm] = terms.join(' ').match(r).length;
	}	

	return table;
}

function Document(data) {
	// Private properties.
	this._tf = computeTermFrequencyTable(data.terms);
	this._M = data.terms.length;
	this._name = data.name;

	console.log(`Loaded document ${this._name}`);
}

Document.prototype.getName = function() {
	return this._name;
}

Document.prototype.getTF = function(term) {
	return this._tf[term] || 0;
}

Document.prototype.getTotalWords = function() {
	return this._M;
}

Document.prototype.containsTerm = function(term) {
	return this._tf[term] !== undefined;
}

module.exports = Document;